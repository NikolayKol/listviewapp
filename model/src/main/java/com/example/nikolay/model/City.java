package com.example.nikolay.model;

import java.io.Serializable;
import java.util.UUID;

/**
 * Created by nikolay on 02.08.15.
 */
public class City implements Serializable {

    private String city;
    private String country;
    private long population;
    private UUID id;
    private boolean cheked;

    public City(String city, String country, long population, UUID id) {
        this.city = city;
        this.country = country;
        this.population = population;
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public String getCity() {
        return city;
    }


    public String getCountry() {
        return country;
    }

    public long getPopulation() {
        return population;
    }

    public void setPopulation(long population) {
        this.population = population;
    }

    public boolean isCheked() {
        return cheked;
    }

    public void setCheked(boolean cheked) {
        this.cheked = cheked;
    }

}
