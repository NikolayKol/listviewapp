package com.example.nikolay.model;

import android.content.Context;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

/**
 * Created by nikolay on 02.08.15.
 */
public class Model {

    private long commonPopulation;
    private List<City> cities;
    private String filename = "cities";
    private Context app;

    public Model(Context app) {
        this.app = app;
        load();
        if (cities == null) firstInit();
    }

    public long calculateCommonPopulation() {

        commonPopulation = 0;

        for (City item : cities) {
            if (item.isCheked())
                commonPopulation += item.getPopulation();
        }
        return commonPopulation;
    }

    public List<City> getCities() {
        return cities;
    }

    public void updatePopulation(String population, int position) {
        City city = getCity(position);
        city.setPopulation(Long.parseLong(population));
        calculateCommonPopulation();
        save();
    }

    public City getCity(int position) {
        return cities.get(position);
    }

    public City getCity(UUID uuid) {
        for (City city : cities) {
            if (city.getId().equals(uuid))
                return city;
        }
        return null;
    }

    public List<City> getCheckedCities() {
        List<City> checkedCities = new ArrayList<>();
        for (City city : cities) {
            if (city.isCheked())
                checkedCities.add(city);
        }

        Comparator<City> comparator = new Comparator<City>() {
            @Override
            public int compare(City lhs, City rhs) {
                return (int) rhs.getPopulation() - (int)lhs.getPopulation();
            }
        };

        Collections.sort(checkedCities, comparator);

        return checkedCities;
    }

    private void firstInit() {
        cities = new ArrayList<City>();

        cities.add(new City("Токио", "Япония", 34500000, UUID.randomUUID()));
        cities.add(new City("Гуанчжоу", "Китай", 25800000, UUID.randomUUID()));
        cities.add(new City("Джакарта", "Индонезия", 25300000, UUID.randomUUID()));
        cities.add(new City("Сеул", "Корея", 25300000, UUID.randomUUID()));
        cities.add(new City("Шанхай", "Китай", 25300000, UUID.randomUUID()));
        cities.add(new City("Мехико", "Мексика", 23200000, UUID.randomUUID()));
        cities.add(new City("Дели", "Индия", 23000000, UUID.randomUUID()));
        cities.add(new City("Нью-Йорк", "США", 21500000, UUID.randomUUID()));
        cities.add(new City("Сан-Пауло", "Бразилия", 2110000, UUID.randomUUID()));
        cities.add(new City("Карачи", "Пакистан", 21100000, UUID.randomUUID()));
        cities.add(new City("Бомбей", "Индия", 20800000, UUID.randomUUID()));
        cities.add(new City("Манила", "Филиппины", 20700000, UUID.randomUUID()));
        cities.add(new City("Лос-Анжелес", "США", 17000000, UUID.randomUUID()));
        cities.add(new City("Осака", "Япония", 16800000, UUID.randomUUID()));
        cities.add(new City("Пекин", "Китай", 16400000, UUID.randomUUID()));
    }

    private void save() {
        try {
            FileOutputStream fos = app.openFileOutput(filename, Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(cities);
            oos.close();

        } catch (Exception e) {
        }
    }

    private void load() {
        try {
            FileInputStream fis = app.openFileInput(filename);
            ObjectInputStream ois = new ObjectInputStream(fis);
            cities = (ArrayList<City>)ois.readObject();
            ois.close();
        } catch (Exception e) {
        }
    }

}
