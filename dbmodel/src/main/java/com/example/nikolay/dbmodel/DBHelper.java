package com.example.nikolay.dbmodel;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


/**
 * Created by nikolay on 11.08.15.
 */
public class DBHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "mydatabase.db";
    private static final int DATABASE_VERSION = 1;
    static final String DATABASE_TABLE = "cities";

    public static final String CITY_COLUMN = "city";
    public static final String COUNTRY_COLUMN = "country";
    public static final String POPULATION_COLUMN = "population";
    public static final String ID_COLUMN = "id";
    public static final String CHECK_COLUMN = "ischecked";

    private static final String DATABASE_CREATE_SCRIPT = "create table "
            + DATABASE_TABLE + " (" + ID_COLUMN
            + " text not null, " + CITY_COLUMN
            + " text not null, " + COUNTRY_COLUMN
            + " text not null, " + POPULATION_COLUMN + " integer, " + CHECK_COLUMN
            + " text not null);";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DATABASE_CREATE_SCRIPT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
