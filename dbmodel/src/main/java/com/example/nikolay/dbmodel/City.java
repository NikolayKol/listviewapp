package com.example.nikolay.dbmodel;

import java.util.UUID;

/**
 * Created by nikolay on 11.08.15.
 */
public class City {
    private String city;
    private String country;
    private long population;
    private UUID id;
    private boolean checked;

    public City(String city, String country, long population, UUID id) {
        this.city = city;
        this.country = country;
        this.population = population;
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public String getCity() {
        return city;
    }


    public String getCountry() {
        return country;
    }

    public long getPopulation() {
        return population;
    }

    public void setPopulation(long population) {
        this.population = population;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
