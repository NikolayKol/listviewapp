package com.example.nikolay.dbmodel;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

/**
 * Created by nikolay on 11.08.15.
 */
public class Model {
    Context context;
    SQLiteDatabase sdb;

    public Model(Context context) {
        this.context = context;
        DBHelper dbHelper = new DBHelper(context);
        sdb = dbHelper.getReadableDatabase();
    }

    public long calculateCommonPopulation() {
        long common = 0;

        for (City city : getCities()) {
            common += city.getPopulation();
        }

        return common;
    }

    public void addCity(City city) {

        ContentValues values = new ContentValues();

        Boolean noChecked = false;

        values.put(DBHelper.CITY_COLUMN, city.getCity());
        values.put(DBHelper.COUNTRY_COLUMN, city.getCountry());
        values.put(DBHelper.POPULATION_COLUMN, (int)city.getPopulation());
        values.put(DBHelper.ID_COLUMN, city.getId().toString());
        values.put(DBHelper.CHECK_COLUMN, noChecked.toString());
        sdb.insert(DBHelper.DATABASE_TABLE, null, values);

    }

    public List<City> getCities() {
        List<City> cities = new ArrayList<City>();

        Cursor cursor = sdb.rawQuery("select * from " + DBHelper.DATABASE_TABLE, null);
        if (!cursor.moveToFirst()) return cities;
        do {
            String cityName = cursor.getString(cursor.getColumnIndex(DBHelper.CITY_COLUMN));
            String country = cursor.getString(cursor.getColumnIndex(DBHelper.COUNTRY_COLUMN));
            int population = cursor.getInt(cursor.getColumnIndex(DBHelper.POPULATION_COLUMN));
            String uuid = cursor.getString(cursor.getColumnIndex(DBHelper.ID_COLUMN));
            String checked = cursor.getString(cursor.getColumnIndex(DBHelper.CHECK_COLUMN));
            City city = new City(cityName, country, population, UUID.fromString(uuid));
            city.setChecked(Boolean.parseBoolean(checked));
            cities.add(city);
        } while (cursor.moveToNext());

        cursor.close();
        return cities;
    }

    public void updatePopulation(String population, UUID uuid) {
        sdb.rawQuery("update " + DBHelper.DATABASE_TABLE + " set " + DBHelper.POPULATION_COLUMN +
                " = " + population + " where " + DBHelper.ID_COLUMN + " = '" + uuid.toString() + "'", null);
    }

    public City getCity(UUID uuid) {

        String query = "select * from " + DBHelper.DATABASE_TABLE + " where " + DBHelper.ID_COLUMN + " = '" + uuid.toString() + "'";
        Cursor cursor = sdb.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            String cityName = cursor.getString(cursor.getColumnIndex(DBHelper.CITY_COLUMN));
            String country = cursor.getString(cursor.getColumnIndex(DBHelper.COUNTRY_COLUMN));
            int population = cursor.getInt(cursor.getColumnIndex(DBHelper.POPULATION_COLUMN));
            String checked = cursor.getString(cursor.getColumnIndex(DBHelper.CHECK_COLUMN));
            City city = new City(cityName, country, population, uuid);
            city.setChecked(Boolean.parseBoolean(checked));
            cursor.close();
            return city;
        }

        return null;
    }

    public List<City> getCheckedCities() {
        List<City> list = new ArrayList<City>();

        for (City city : getCities()) {
            if (city.isChecked())
                list.add(city);
        }

        Comparator<City> comparator = new Comparator<City>() {
            @Override
            public int compare(City lhs, City rhs) {
                return (int) rhs.getPopulation() - (int)lhs.getPopulation();
            }
        };

        Collections.sort(list, comparator);

        return list;
    }

    public void setChecked(UUID uuid, Boolean checked) {
        String setCheckedQuery = "update " + DBHelper.DATABASE_TABLE + " set " + DBHelper.CHECK_COLUMN + " = '" +
                checked.toString() + "' where " + DBHelper.ID_COLUMN + " = '" + uuid.toString() + "'";
        sdb.execSQL(setCheckedQuery);

        Cursor cursor = sdb.rawQuery("select * from " + DBHelper.DATABASE_TABLE + " where " + DBHelper.ID_COLUMN + " = '" + uuid.toString() + "'", null);
        cursor.moveToFirst();

        String check = cursor.getString(cursor.getColumnIndex(DBHelper.CHECK_COLUMN));
        int i;
    }
}
