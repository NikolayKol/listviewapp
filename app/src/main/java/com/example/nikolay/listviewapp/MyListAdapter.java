package com.example.nikolay.listviewapp;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.nikolay.dbmodel.City;

import java.util.List;
import java.util.UUID;


/**
 * Created by nikolay on 02.08.15.
 */
public class MyListAdapter extends BaseAdapter {

    List<City> cities;
    MainActivity mainActivity;

    public MyListAdapter(MainActivity mainActivity, List<City> cities) {
        this.cities = cities;
        this.mainActivity = mainActivity;
    }

    public void setChecked(UUID uuid, boolean isChecked) {
        mainActivity.setChecked(uuid, isChecked);
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }

    @Override
    public int getCount() {
        if (cities == null) return 1;
        return cities.size() + 1;
    }

    @Override
    public Object getItem(int position) {
        return cities.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (position == getCount() - 1) {
            convertView = LayoutInflater.from(mainActivity).inflate(R.layout.add_list_element, null);
            ImageView imageView = (ImageView) convertView.findViewById(R.id.imageViewAdd);
            imageView.setImageResource(R.drawable.add_btn);
            convertView.setTag("addBtn");
            return convertView;
        }

        final City listItem = (City)getItem(position);

        if (convertView == null || convertView.getTag().equals("addBtn")) {
            convertView = LayoutInflater.from(mainActivity).inflate(R.layout.list_element, null);

            CheckBox checkBox = (CheckBox) convertView.findViewById(R.id.city_list_item_countCheckBox);
            checkBox.setTag(position);
            checkBox.setChecked(listItem.isChecked());
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                List<City> cityList = cities;

                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    City item = cityList.get((int) buttonView.getTag());
                    item.setChecked(isChecked);
                    //updateCount(item.getId());
                    setChecked(item.getId(), isChecked);
                }
            });
            convertView.setTag(listItem.getId().toString());
        }else {
            CheckBox checkBox = (CheckBox) convertView.findViewById(R.id.city_list_item_countCheckBox);
            checkBox.setTag(position);
            checkBox.setChecked(listItem.isChecked());
        }

        TextView textView = (TextView) convertView.findViewById(R.id.city);
        textView.setText(listItem.getCity());

        textView = (TextView) convertView.findViewById(R.id.country);
        textView.setText(listItem.getCountry());

        FormatView formatView = (FormatView) convertView.findViewById(R.id.population);
        formatView.setPopulation((int) listItem.getPopulation());

        return convertView;
    }

}
