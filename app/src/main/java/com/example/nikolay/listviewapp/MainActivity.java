package com.example.nikolay.listviewapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.nikolay.dbmodel.City;
import com.example.nikolay.dbmodel.Model;

import java.util.UUID;


public class MainActivity extends Activity {

    MyListAdapter adapter;
    FormatView formatView;
    Model model;
    Button statBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        model = App.getInstance().getModel();
        ListView listView = (ListView)findViewById(R.id.listView);
        adapter = new MyListAdapter(this, model.getCities());
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == adapter.getCount() - 1) {
                    Intent intent = new Intent(MainActivity.this, AddCityActivity.class);
                    startActivity(intent);
                }
                else {
                    Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                    String tag = (String) view.getTag();
                    intent.putExtra("id", tag);
                    //intent.putExtra("pos", position);
                    startActivity(intent);
                }
            }
        });

        statBtn = (Button) findViewById(R.id.button);
        statBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (model.calculateCommonPopulation() == 0) {
                    Toast.makeText(MainActivity.this, "no checked cities", Toast.LENGTH_LONG).show();
                    return;
                }
                Intent intent = new Intent(MainActivity.this, StatisticActivity.class);
                startActivity(intent);
            }
        });

        formatView = (FormatView) findViewById(R.id.formatPopulation);
        formatView.setPopulation((int) model.calculateCommonPopulation());
    }

    @Override
    protected void onStart() {
        super.onStart();
        updateList();
        updatePopulation();
    }

    public void updatePopulation() {
       formatView.setPopulation((int) model.calculateCommonPopulation());
    }

    public void setChecked(UUID uuid, boolean checked) {
        model.setChecked(uuid, checked);
        updatePopulation();
    }

    public void updateList() {
        adapter.setCities(model.getCities());
        adapter.notifyDataSetChanged();
    }

    public void addCity(City city) {
        model.addCity(city);
        updateList();
    }
}
