package com.example.nikolay.listviewapp;

import android.app.Application;
import android.util.Log;

import com.example.nikolay.dbmodel.Model;


/**
 * Created by nikolay on 02.08.15.
 */
public class App extends Application {


    private static App app;
    private Model model;

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
        model = new Model(this);
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable ex) {
                Log.e("uncaught exception", ex.getMessage());
            }
        });
    }


    public static App getInstance() {
        return app;
    }

    public Model getModel() {
        return model;
    }
}
