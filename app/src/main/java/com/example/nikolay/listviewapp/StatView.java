package com.example.nikolay.listviewapp;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;


import com.example.nikolay.dbmodel.City;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nikolay on 07.08.15.
 */
public class StatView extends View  {

    List<City> cities = new ArrayList<>();
    float offset = 0;
    String DEBUG_TAG = "scroll";

    public StatView(Context context) {
        super(context);
    }

    public StatView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public StatView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }

    public void setOffset(float offset) {
        this.offset += offset;
    }

    Paint paint = new Paint();

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int height = getHeight();

        canvas.drawARGB(80, 102, 204, 255);

        if (cities.isEmpty()) return;

        //ToDo: куда это вынести??
        paint.setTextSize(70);
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setColor(Color.BLUE);
        paint.setStrokeWidth(getWidth() / 25);
        paint.setAntiAlias(true);

        //ToDo: сделать для не сортированного списка (getMaxPopulation) посмотреть TextPaint() вынести сеттреы паинт

        float maxPopulation = 0;

        for (City city : cities)
            if (city.getPopulation() > maxPopulation) maxPopulation = city.getPopulation();


        float scale = (float) (height - 400) / (float) cities.get(0).getPopulation();
        int lineStep = (int) paint.getStrokeWidth() * 10;
        int step = lineStep / 2;


        canvas.translate(offset, 0);

        for (City city : cities) {
            canvas.drawLine(step, height - 300, step, height - 300 - (scale * city.getPopulation()) + 100, paint);
            canvas.drawText(parsePopulation(city.getPopulation()), step, 100, paint);
            step += lineStep;
        }

        canvas.translate(getWidth(), 0);
        canvas.rotate(90);

        step = lineStep / 2;
        paint.setTextAlign(Paint.Align.LEFT);

        for (City city : cities) {
            canvas.drawText("    " + city.getCity(), height - 300, getWidth() - step, paint);//убрать пробелы
            step += lineStep;
        }

    }


    Float lastPoint;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction())
        {
            case MotionEvent.ACTION_DOWN:
                lastPoint = event.getRawX();
                break;
            case MotionEvent.ACTION_MOVE:
                setOffset(event.getRawX() - lastPoint);
                lastPoint = event.getRawX();
                Log.d(DEBUG_TAG, lastPoint.toString());
                invalidate();
                break;
        }
        //ToDo: стопер для скролла
        return true;
    }

    public static String parsePopulation(long population) {

        String parsedPopulation = null;

        if (population % 1000 != 0) parsedPopulation = Long.toString(population);
        else if (population % 1000000 == 0) parsedPopulation = Long.toString(population / 1000000) + " млн.";
        else parsedPopulation = Long.toString(population / 1000000) + " млн. " + Long.toString((population % 1000000) / 1000) + " тыс.";

        return parsedPopulation;
    }
}
