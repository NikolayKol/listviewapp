package com.example.nikolay.listviewapp;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.nikolay.dbmodel.City;
import com.example.nikolay.dbmodel.Model;

import java.util.UUID;


public class SecondActivity extends Activity {

    Model model;
    City item;
    //int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        model = App.getInstance().getModel();
        //position = getIntent().getIntExtra("pos", 0);
        UUID uuid = UUID.fromString(getIntent().getStringExtra("id"));
        item = model.getCity(uuid);

        final EditText editText = (EditText)findViewById(R.id.editText);
        editText.setHint(Long.toString(item.getPopulation()));

        Button button = (Button)findViewById(R.id.okButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String newPopulation = editText.getText().toString();
                if (!newPopulation.equals(""))
                {
                    updatePopulation(newPopulation);
                }
                finish();
            }
        });
    }

    void updatePopulation(String population) {
        model.updatePopulation(population, item.getId());
    }

}
