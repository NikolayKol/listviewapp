package com.example.nikolay.listviewapp;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.nikolay.dbmodel.City;

import java.util.UUID;


public class AddCityActivity extends Activity {

    EditText editCity;
    EditText editCountry;
    EditText editPopulation;
    Button saveBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_city);
        editCity = (EditText) findViewById(R.id.editTextCity);
        editCountry = (EditText) findViewById(R.id.editTextCountry);
        editPopulation = (EditText) findViewById(R.id.editTextPopulation);

        saveBtn = (Button) findViewById(R.id.buttonSaveCity);
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String city = editCity.getText().toString();
                String country = editCountry.getText().toString();
                String population = editPopulation.getText().toString();

                if (city.isEmpty() || country.isEmpty() || population.isEmpty())
                {
                    Toast.makeText(AddCityActivity.this, "set all fields", Toast.LENGTH_SHORT).show();
                    return;
                }

                City c = new City(city, country, Long.parseLong(population), UUID.randomUUID());
                ((App)getApplication()).getModel().addCity(c);
                finish();

            }
        });
    }
}
