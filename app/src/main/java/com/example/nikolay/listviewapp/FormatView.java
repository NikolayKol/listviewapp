package com.example.nikolay.listviewapp;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by nikolay on 10.08.15.
 */
public class FormatView extends TextView {

    public FormatView(Context context) {
        super(context);
    }

    public FormatView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FormatView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    int population;

    public void setPopulation(int population) {
        this.population = population;

        String formattedPopulation = null;

        if (population % 1000 != 0) formattedPopulation = Long.toString(population);
        else if (population % 1000000 == 0) formattedPopulation = Long.toString(population / 1000000) + " млн.";
        else formattedPopulation = Long.toString(population / 1000000) + " млн. " + Long.toString((population % 1000000) / 1000) + " тыс.";

        setText("Население:\n" + formattedPopulation);

    }

}
