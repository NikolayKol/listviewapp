package com.example.nikolay.listviewapp;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.example.nikolay.dbmodel.Model;


public class StatisticActivity extends Activity {

    StatView statView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i("Activity", "onCreate stat");
        setContentView(R.layout.activity_statistic);
        statView = (StatView) findViewById(R.id.statView);
        statView.setCities(getModel().getCheckedCities());

        FormatView formatView = (FormatView) findViewById(R.id.formatView);
        formatView.setPopulation((int) getModel().calculateCommonPopulation());
    }

    Model getModel() {
        return ((App)getApplication()).getModel();
    }

}
